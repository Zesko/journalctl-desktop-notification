## Journalctl Desktop Notification

It notifies you on your desktop whenever an error message appears in the Journalctl log.

### Screenshots

Here's an example using a test command:

```
$ logger -p err "BTRFS error test"
```

KDE:

![image](screenshots/1.png)

GNOME:

![image](screenshots/2.png)



### Requirements
* **libnotify**: Install for notification support.
* **Systemd**: Required for journalctl.
* **openssh**: Needed for remote monitoring on another host using SSH keys (without a password).
* A **systemd-based** Linux distribution.

### Installation

* Arch Linux: Install from the [AUR](https://aur.archlinux.org/packages/journalctl-desktop-notification/).


### Configuration file

By default, it loads `~/.config/journalctl-desktop-notification.conf`.
If unavailable, it falls back to `~/.config/journalctl-desktop-notification/local.conf`

If neither is found, it uses the system configuration file at`/etc/journalctl-desktop-notification.conf`.

If you want to create a custom configuration (e.g., `/home/user/.config/your_server.conf`), run a command line:
``` 
$ journalctl-desktop-notification -c <CONFIG_PATH>

### For example:
$ journalctl-desktop-notification -c "/home/user/.config/your_server.conf"
```

You can run multiple instances of the app, each with a different configuration file.


### Configuration options:

* Exclude spam errors:
```
ERROR_FILTER="Spam1|Spam2|Spam3"
```

* Time interval for checking journalctl (in seconds):
```
TIME_INTERVAL="3"
```

* Custom terminal application and its argument e.g., "konsole -e" for KDE or "gnome-terminal -- bash -c" for GNOME:

```
TERMINAL="gnome-terminal"
TERMINAL_ARG="-- bash -c"
```

* Log level (Warning: 4, Error: 3):
```
LOG_LEVEL="3"
```

* Custom error message icon:
```
ERROR_ICON="/usr/share/icons/breeze-dark/status/24/data-warning.svg"
```

* Remote monitoring log on your server:
   - Enable SSH service and configure SSH keys for passwordless login on both the client and server. Specify the server’s IPv4/IPv6 address or hostname.
   - To deactivate remote monitoring, set `REMOTE_HOST` to blank or comment it out.
 
```
REMOTE_HOST="$USERNAME@$HOSTNAME"

### For example:
REMOTE_HOST="yourName@127.0.0.1"

### Turn off remote monitoring
REMOTE_HOST=""
```

* Custom notification description:
```
DESCRIPTION="Journalctl messages"
```


### Autostart

To enable autostart, copy the desktop entry:
```
$ cp /usr/share/applications/journalctl-desktop-notification.desktop ~/.config/autostart/
```

If you're using a custom configuration located in a path of your choice, modify the autostart desktop file `~/.config/autostart/journalctl-desktop-notification.desktop`:

Change:
```
Exec=/usr/bin/journalctl-desktop-notification
```
to 
```
Exec=/usr/bin/journalctl-desktop-notification -c <CONFIG_PATH>
```


### Troubleshooting

**Issue:** KDE notification is replaced with "knopwob dunst" or an ugly notification after some update.

**Solution:** Switch back to KDE's default notification service:

```
$ mkdir -p ~/.local/share/dbus-1/services/
$ ln -s /usr/share/dbus-1/services/org.kde.plasma.Notifications.service \
  ~/.local/share/dbus-1/services/org.kde.plasma.Notifications.service
```

Alternatively, delete the conflicting service:

```
$ sudo rm /usr/share/dbus-1/services/org.knopwob.dunst.service
```

### Donate

The project is free to use, but donations are welcome. Here are some ways to support:

[<img src="https://gitlab.com/Zesko/resources/-/raw/main/kofiButton.png" width=110px>](https://ko-fi.com/zeskotron) &nbsp;[<img src="https://gitlab.com/Zesko/resources/-/raw/main/PayPalButton.png" width=160px/>](https://www.paypal.com/donate/?hosted_button_id=XKP36D62AY5HY)

