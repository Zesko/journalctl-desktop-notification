# Changelog

## 1.4.0 - 2024-12-24

### Added

- Added support for `libnotify` as an alternative to `dunst`
- Support for loading default config files from `~/.config/journalctl-desktop-notification.conf` or `~/.config/journalctl-desktop-notification/local.conf`.

## 1.3.0 - 2024-12-11

### Removed

- Removed unnecessary `/usr/share/doc/`, now the PKGBUILD handles copying documentation files to the appropriate directory for Arch Linux.

## 1.2.0 - 2024-12-10

### Added

- Default terminals for better compatibility.
- Config: Introduced `TERMINAL_ARG` to specify terminal options.
- Added a changelog.

### Changed

- Simplified the bash code.
- Updated the README

## 1.1.0 - 2024-11-20

### Added

- Automatic reloading of configurations when changed, without requiring a restart.
- A function to display the version.

## 1.0.1 - 2024-11-20

### Fixed

- Removed `/usr/bin/` binary paths to ensure compatibility with non-FHS (Filesystem Hierarchy Standard) systems.
- Changed the default shebang to `#!/usr/bin/env bash` for improved compatibility.

## 1.0.0

### Added

- Initial release.

